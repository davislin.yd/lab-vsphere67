# 要開始準備測試了！
---

## 需要的設備

一台 Windows PC 或是 Mac，但建議使用 Windows，至於硬體規格建議如下：

**CPU**

- 一定要有虛擬化功能：Intel VT or AMD-V。
- 時脈、核心則是愈高愈好。
- 若是 CPU 具備 Hyper-Threading 的話就打開它吧，但沒有也沒關係。

**RAM**

- ESXi server 以及 VCSA 在安裝時都有最低記憶體的要求，所以建議總共要有 24 GB 以上。
- ESXi server 最低需要 4 GB RAM；VCSA 最低需要 10 GB RAM。

**Network**

- 這次的測試即便在斷網的情況下還是可以正常運作，但為了測試方便，還是強烈建議需要具備上網能力。
- 在下一個章節中會提到這次測試的架構圖，其中管理網段會和實體 Windows 網路相同，因此建議具有管理實體網路的權限。$$

---

## 需要的軟體

### Hypervisor (虛擬化平台)

  - Oracle VirtualBox 或 VMware Workstation 擇一即可：

    - VMware Workstation 是付費軟體，在沒有授權的情況下有 XX 天的試用期。

      - 這次的測試以 VMware Workstation 為測試平台。

    - Oracle VirtualBox 是免費軟體 (僅限個人使用)，但要 6.0 以上版本才具體 Nested Virtualization 的功能。

      *註：截至 Jan 25, 2020 為止，目前 VirtualBox 的 Nested Virtualization 僅支援 AMD CPU。*

### VMware vSphere 相關安裝檔

  - 於 VMware 官網註冊帳號後，到下列連結下載需要的安裝檔：

  - ESXi: VMware vSphere Hypervisor (ESXi) 6.7U2 (or later version)
  - vCenter: VMware vCenter Server 6.7U2 (or later version)

### 其它工具

  - 任何支援 Windows Remote Desktop 協定的遠端連線工具。(非必要)
  - 任何支援 SSH 協定的遠端連線工具：(非必要)
    - 若你的 Windows 版本高於 1809，則可以考慮用 PowerShell 中的 ssh 指令。 參考連結：[安裝適用於 Windows 的 OpenSSH](https://docs.microsoft.com/zh-tw/windows-server/administration/openssh/openssh_install_firstuse)



## 需要具備的知識

### 網路方面：

- 明白 [OSI 模型](https://zh.wikipedia.org/zh-tw/OSI%E6%A8%A1%E5%9E%8B)是什麼東西
  - OSI Layer 2 (Data-Link)
    - 什麼是橋接器 (Bridge) / 交換器 (Switch)？它們差在哪？
    - VLAN 是什麼？它是怎麼運作的呢？
    - Trunk 和 VLAN 之間的關係？
  - OSI Layer 3 (Network)
    - 什麼是路由器 (Router) 和三層交換器 (L3 Switch)？
    - IP address、Subnet mask、Gateway 之類的關係。
    - Gateway 和 Default gateway 是不一樣的。
    - NAT 怎麼運作？
      - SNAT 和 DNAT 差在哪？
  - OSI Layer 4 (Transport)
    - TCP 和 UDP 差在哪？
    - 熟悉各種常用的服務埠號 (port)。

### 硬體方面：(待補齊)

- CPU
  - 核心
  - 時脈
- RAM
- HDD
